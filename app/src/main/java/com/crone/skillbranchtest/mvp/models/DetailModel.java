package com.crone.skillbranchtest.mvp.models;

import com.crone.skillbranchtest.data.managers.DataManager;

/**
 * Created by CRN_soft on 31.10.2016.
 */

public class DetailModel {

    private DataManager mDataManager;

    public DetailModel(){
        mDataManager = DataManager.getInstance();
    }

    public void findInfoById(int houseId){
        mDataManager.findInfoById(houseId);
    }
}
